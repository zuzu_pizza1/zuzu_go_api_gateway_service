package util

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIsValidUUID(t *testing.T) {
	t.Run("Normal", func(t *testing.T) {
		require.True(t, IsValidUUID("78144eb0-89c1-47f9-9dae-fd552330af46"))
	})

	t.Run("Error", func(t *testing.T) {
		require.False(t, IsValidUUID("teapot"))
		require.False(t, IsValidUUID(""))
	})
}
