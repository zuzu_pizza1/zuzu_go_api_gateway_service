package client

import (

	"zuzu_go_api_gateway/config"
	"zuzu_go_api_gateway/genproto/catalog_service"
	"zuzu_go_api_gateway/genproto/order_service"
	"zuzu_go_api_gateway/genproto/payment_service"

	"zuzu_go_api_gateway/genproto/user_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	ProductService() catalog_service.ProductServiceClient
	CategoryService() catalog_service.CategoryServiceClient
	OrderService() order_service.OrderServiceClient
	ShopcartService() order_service.ShopcartServiceClient
	PaymentService() payment_service.PaymentServiceClient
}

type grpcClients struct {
	userService     user_service.UserServiceClient
	productService  catalog_service.ProductServiceClient
	categoryService catalog_service.CategoryServiceClient
	orderService    order_service.OrderServiceClient
	shopcartService order_service.ShopcartServiceClient
	paymentService  payment_service.PaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// maxcallMsgSize := grpc.WithDefaultCallOptions(grpc.MaxCallSendMsgSize(1024 * 1024 * 1024))

	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceHost+cfg.CatalogGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	connPaymentService, err := grpc.Dial(
		cfg.PaymentServiceHost+cfg.PaymentGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:     user_service.NewUserServiceClient(connUserService),
		productService:  catalog_service.NewProductServiceClient(connCatalogService),
		categoryService: catalog_service.NewCategoryServiceClient(connCatalogService),
		orderService:    order_service.NewOrderServiceClient(connOrderService),
		shopcartService: order_service.NewShopcartServiceClient(connOrderService),
		paymentService:  payment_service.NewPaymentServiceClient(connPaymentService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ShopcartService() order_service.ShopcartServiceClient {
	return g.shopcartService
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}

func (g *grpcClients) PaymentService() payment_service.PaymentServiceClient {
	return g.paymentService
}
