package handlers

import (
	"context"
	"fmt"
	"zuzu_go_api_gateway/api/http"
	"zuzu_go_api_gateway/api/models"
	"zuzu_go_api_gateway/genproto/order_service"
	"zuzu_go_api_gateway/pkg/helper"
	"zuzu_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateShopcart godoc
// @ID create_shopcart
// @Router /shopcart [POST]
// @Summary Create Shopcart
// @Description  Create Shopcart
// @Tags Shopcart
// @Accept json
// @Produce json
// @Param profile body order_service.CreateShopcart true "CreateShopcartRequestBody"
// @Success 200 {object} http.Response{data=order_service.Shopcart} "GetShopcartBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShopcart(c *gin.Context) {

	fmt.Println("afaddf")

	var shopcart order_service.CreateShopcart

	err := c.ShouldBindJSON(&shopcart)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ShopcartService().Create(
		c.Request.Context(),
		&shopcart,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetShopcartByID godoc
// @ID get_shopcart_by_id
// @Router /shopcart/{id} [GET]
// @Summary Get Shopcart  By ID
// @Description Get Shopcart  By ID
// @Tags Shopcart
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=order_service.Shopcart} "ShopcartBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShopcartByID(c *gin.Context) {

	shopcartID := c.Param("id")

	if !util.IsValidUUID(shopcartID) {
		h.handleResponse(c, http.InvalidArgument, "shopcart id is an invalid uuid")
		return
	}

	resp, err := h.services.ShopcartService().GetById(
		context.Background(),
		&order_service.ShopcartPrimaryKey{
			ShopcartId: shopcartID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetShopcartList godoc
// @ID get_shopcart_list
// @Router /shopcart [GET]
// @Summary Shopcart  List
// @Description  Get Shopcart  List
// @Tags Shopcart
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=order_service.GetListShopcartResponse} "GetAllShopcartResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShopcartList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ShopcartService().GetList(
		context.Background(),
		&order_service.GetListShopcartRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateShopcart godoc
// @ID update_shopcart
// @Router /shopcart/{id} [PUT]
// @Summary Update Shopcart
// @Description Update Shopcart
// @Tags Shopcart
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body order_service.UpdateShopcart true "UpdateShopcartRequestBody"
// @Success 200 {object} http.Response{data=order_service.Shopcart} "Shopcart data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShopcart(c *gin.Context) {

	var shopcart order_service.UpdateShopcart
	shopcart.ShopcartId = c.Param("id")

	if !util.IsValidUUID(shopcart.ShopcartId) {
		h.handleResponse(c, http.InvalidArgument, "shopcart id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&shopcart)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ShopcartService().Update(
		c.Request.Context(),
		&shopcart,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchShopcart godoc
// @ID patch_shopcart
// @Router /shopcart/{id} [PATCH]
// @Summary Patch Shopcart
// @Description Patch Shopcart
// @Tags Shopcart
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=order_service.Shopcart} "Shopcart data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchShopcart(c *gin.Context) {

	var updatePatchShopcart models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchShopcart)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchShopcart.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchShopcart.ID) {
		h.handleResponse(c, http.InvalidArgument, "shopcart id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchShopcart.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ShopcartService().UpdatePatch(
		c.Request.Context(),
		&order_service.UpdatePatchShopcart{
			Id:     updatePatchShopcart.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteShopcart godoc
// @ID delete_shopcart
// @Router /shopcart/{id} [DELETE]
// @Summary Delete Shopcart
// @Description Delete Shopcart
// @Tags Shopcart
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Shopcart data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShopcart(c *gin.Context) {

	shopcartId := c.Param("id")

	if !util.IsValidUUID(shopcartId) {
		h.handleResponse(c, http.InvalidArgument, "shopcart id is an invalid uuid")
		return
	}

	resp, err := h.services.ShopcartService().Delete(
		c.Request.Context(),
		&order_service.ShopcartPrimaryKey{ShopcartId: shopcartId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
