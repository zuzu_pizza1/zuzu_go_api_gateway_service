package handlers

import (
	"context"
	"fmt"
	"zuzu_go_api_gateway/api/http"
	"zuzu_go_api_gateway/api/models"
	"zuzu_go_api_gateway/genproto/catalog_service"
	"zuzu_go_api_gateway/pkg/helper"
	"zuzu_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateProduct godoc
// @ID create_product
// @Router /product [POST]
// @Summary Create Product
// @Description  Create Product
// @Tags Product
// @Accept json
// @Produce json
// @Param profile body catalog_service.CreateProduct true "CreateProductRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Product} "GetProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateProduct(c *gin.Context) {

	fmt.Println("afaddf")

	var product catalog_service.CreateProduct

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Create(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetProductByID godoc
// @ID get_product_by_id
// @Router /product/{id} [GET]
// @Summary Get Product  By ID
// @Description Get Product  By ID
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=catalog_service.Product} "ProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductByID(c *gin.Context) {

	productID := c.Param("id")

	if !util.IsValidUUID(productID) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().GetById(
		context.Background(),
		&catalog_service.ProductPrimaryKey{
			ProductId: productID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetProductList godoc
// @ID get_product_list
// @Router /product [GET]
// @Summary Product  List
// @Description  Get Product  List
// @Tags Product
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=catalog_service.GetListProductResponse} "GetAllProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetProductList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().GetList(
		context.Background(),
		&catalog_service.GetListProductRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateProduct godoc
// @ID update_product
// @Router /product/{id} [PUT]
// @Summary Update Product
// @Description Update Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body catalog_service.UpdateProduct true "UpdateProductRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateProduct(c *gin.Context) {

	var product catalog_service.UpdateProduct
	product.ProductId = c.Param("id")

	if !util.IsValidUUID(product.ProductId) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&product)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Update(
		c.Request.Context(),
		&product,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchProduct godoc
// @ID patch_product
// @Router /product/{id} [PATCH]
// @Summary Patch Product
// @Description Patch Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Product} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchProduct(c *gin.Context) {

	var updatePatchProduct models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchProduct)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchProduct.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchProduct.ID) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchProduct.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.ProductService().UpdatePatch(
		c.Request.Context(),
		&catalog_service.UpdatePatchProduct{
			Id:     updatePatchProduct.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteProduct godoc
// @ID delete_product
// @Router /product/{id} [DELETE]
// @Summary Delete Product
// @Description Delete Product
// @Tags Product
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Product data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteProduct(c *gin.Context) {

	productId := c.Param("id")

	if !util.IsValidUUID(productId) {
		h.handleResponse(c, http.InvalidArgument, "product id is an invalid uuid")
		return
	}

	resp, err := h.services.ProductService().Delete(
		c.Request.Context(),
		&catalog_service.ProductPrimaryKey{ProductId: productId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
