package handlers

import (
	"context"
	"fmt"
	"zuzu_go_api_gateway/api/http"
	"zuzu_go_api_gateway/api/models"
	"zuzu_go_api_gateway/genproto/catalog_service"
	"zuzu_go_api_gateway/pkg/helper"
	"zuzu_go_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// CreateCategory godoc
// @ID create_category
// @Router /category [POST]
// @Summary Create Category
// @Description  Create Category
// @Tags Category
// @Accept json
// @Produce json
// @Param profile body catalog_service.CreateCategory true "CreateCategoryRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Category} "GetCategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCategory(c *gin.Context) {

	fmt.Println("afaddf")

	var category catalog_service.CreateCategory

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Create(
		c.Request.Context(),
		&category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetCategoryByID godoc
// @ID get_category_by_id
// @Router /category/{id} [GET]
// @Summary Get Category  By ID
// @Description Get Category  By ID
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=catalog_service.Category} "CategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryByID(c *gin.Context) {

	categoryID := c.Param("id")

	if !util.IsValidUUID(categoryID) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().GetById(
		context.Background(),
		&catalog_service.CategoryPrimaryKey{
			CategoryId: categoryID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetCategoryList godoc
// @ID get_category_list
// @Router /category [GET]
// @Summary Category  List
// @Description  Get Category  List
// @Tags Category
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=catalog_service.GetListCategoryResponse} "GetAllCategoryResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCategoryList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CategoryService().GetList(
		context.Background(),
		&catalog_service.GetListCategoryRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateCategory godoc
// @ID update_category
// @Router /category/{id} [PUT]
// @Summary Update Category
// @Description Update Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body catalog_service.UpdateCategory true "UpdateCategoryRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Category} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCategory(c *gin.Context) {

	var category catalog_service.UpdateCategory
	category.CategoryId = c.Param("id")

	if !util.IsValidUUID(category.CategoryId) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.CategoryService().Update(
		c.Request.Context(),
		&category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchCategory godoc
// @ID patch_category
// @Router /category/{id} [PATCH]
// @Summary Patch Category
// @Description Patch Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Category} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchCategory(c *gin.Context) {

	var updatePatchCategory models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchCategory)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchCategory.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchCategory.ID) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchCategory.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.CategoryService().UpdatePatch(
		c.Request.Context(),
		&catalog_service.UpdatePatchCategory{
			Id:     updatePatchCategory.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteCategory godoc
// @ID delete_category
// @Router /category/{id} [DELETE]
// @Summary Delete Category
// @Description Delete Category
// @Tags Category
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCategory(c *gin.Context) {

	categoryId := c.Param("id")

	if !util.IsValidUUID(categoryId) {
		h.handleResponse(c, http.InvalidArgument, "category id is an invalid uuid")
		return
	}

	resp, err := h.services.CategoryService().Delete(
		c.Request.Context(),
		&catalog_service.CategoryPrimaryKey{CategoryId: categoryId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
